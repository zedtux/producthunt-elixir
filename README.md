# ProductHunt Elixir

The aim of this repository is to clone (more or less) http://producthunt.com/
with [Elixir](http://elixir-lang.github.io) and the
[Phoenix](http://phoenixframework.org) framework.

I think it's a good candidate for a first application:

 - User authentication
 - Administration area
 - Voting system
 - Comments

It's a bit harder than a blog, but easier than almost all other apps.

## Usage

```bash
# Build the Docker image
$ docker-compose build
# Start the Docker sync stack
$ docker-sync-stack start
```

In a new terminal:

```bash
# Install the dependencies
$ docker-compose run --rm -v app-sync:/application app mix deps.get
```
