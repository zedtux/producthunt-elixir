defmodule ProducthuntElixirWeb.ProductController do
  use ProducthuntElixirWeb, :controller

  plug Coherence.Authentication.Session, [protected: true] when action != :index

  def index(conn, _params) do
    render conn, "index.html"
  end

  def new(conn, _params) do
    render conn, "new.html"
  end
end
