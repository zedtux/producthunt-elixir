# Latest version of Erlang-based Elixir installation: https://hub.docker.com/_/elixir/
# Linux distribution: Debian Jessie
FROM elixir:1.5.2-slim
MAINTAINER zedtux, zedtux@zedroot.org

# Create and set home directory
ENV APP_HOME /application
WORKDIR $APP_HOME

# Update APT sources
RUN apt-get update

# Install cURL and bzip2
RUN apt-get install -y curl bzip2 libfontconfig1 build-essential

# Install Nodejs and NPM
# Version 9 is not able to install the fsevents package
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs inotify-tools

ENV PHANTOMJS_VERSION 2.1.1

# Install PhantomJS
RUN cd /tmp && \
    curl -L -O -k https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 && \
    tar xjf /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 -C /tmp && \
    mv /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64/bin/phantomjs /usr/local/bin && \
    rm -rf /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64

# Install hex (Elixir package manager)
RUN mix local.hex --force

# Install rebar (Erlang build tool)
RUN mix local.rebar --force

# Copy all dependencies files
COPY mix.* ./

# Install all dependencies
RUN mix deps.get

# Compile all dependencies
RUN mix deps.compile

# Copy all application files
COPY . .

# Compile the entire project
RUN mix compile

# Run Ecto migrations and Phoenix server as an initial command
CMD mix do ecto.migrate, phx.server
