ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(ProducthuntElixir.Repo, :manual)

{:ok, _} = Application.ensure_all_started(:wallaby)
Application.put_env(:wallaby, :base_url, ProducthuntElixirWeb.Endpoint.url)
Application.put_env(:wallaby, :screenshot_on_failure, true)
