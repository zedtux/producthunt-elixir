defmodule ProducthuntElixir.FeatureCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Wallaby.DSL

      alias ProducthuntElixir.Repo
      import Ecto
      import Ecto.Changeset
      import Ecto.Query

      import ProducthuntElixirWeb.Router.Helpers

      import ProducthuntElixirWeb.Gettext

      def dump_html_to(session, filename) do
        File.write(
          filename,
          Wallaby.Element.attr(
            find(session, Wallaby.Query.css("html")),
            "outerHTML"
          )
        )
        session
      end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(ProducthuntElixir.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(ProducthuntElixir.Repo, {:shared, self()})
    end

    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(ProducthuntElixir.Repo, self())
    {:ok, session} = Wallaby.start_session(metadata: metadata)
    session = Wallaby.Browser.resize_window(session, 993, 700)
    {:ok, session: session}
  end
end
