defmodule ProducthuntElixirWeb.RegisterAProductTest do
  use ProducthuntElixir.FeatureCase, async: true

  import Wallaby.Query

  def signup_new_user(session) do
    session
    |> visit("/")
    |> click(link("Sign up"))
    |> fill_in(text_field("Name"), with: "Mike")
    |> fill_in(text_field("Email"), with: "mike@test.lu")
    |> fill_in(text_field("Password Confirmation"), with: "secret")
    |> fill_in(text_field("registration_password"), with: "secret")
    |> click(button("Register"))
    |> assert_has(css(".alert.alert-info", text: "Registration created successfully."))
  end

  def login_as_new_user(session) do
    signup_new_user(session)

    session
    |> click(link("Login"))
    |> fill_in(text_field("Email"), with: "mike@test.lu")
    |> fill_in(text_field("Password"), with: "secret")
    |> click(button("Login"))
  end

  test "sign up to producthunt", %{session: session} do
    login_as_new_user(session)

    session
    |> dump_html_to("screenshots/#{:os.system_time(:seconds)}.html")
    |> click(link("Register product"))
    |> assert_has(css("h1", text: dgettext("producthunt", "Post something new")))
    |> fill_in(text_field("Link"), with: "Mike")
  end
end
