# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :producthunt_elixir,
  ecto_repos: [ProducthuntElixir.Repo]

# Configures the endpoint
config :producthunt_elixir, ProducthuntElixirWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "79fE1dF6w7cz8/CZ9Z52JzJWlit4lPP7B2llXlhWcO9VmvRE31m+WETxRCD/B9na",
  render_errors: [view: ProducthuntElixirWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ProducthuntElixir.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :producthunt_elixir, env: Mix.env

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: ProducthuntElixir.Coherence.User,
  repo: ProducthuntElixir.Repo,
  module: ProducthuntElixir,
  web_module: ProducthuntElixirWeb,
  router: ProducthuntElixirWeb.Router,
  messages_backend: ProducthuntElixirWeb.Coherence.Messages,
  logged_out_url: "/",
  email_from_name: "Your Name",
  email_from_email: "yourname@example.com",
  opts: [:authenticatable, :recoverable, :lockable, :trackable, :unlockable_with_token, :invitable, :registerable]

config :coherence, ProducthuntElixirWeb.Coherence.Mailer,
  adapter: Swoosh.Adapters.Sendgrid,
  api_key: "your api key here"
# %% End Coherence Configuration %%
