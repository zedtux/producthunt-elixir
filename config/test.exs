use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :producthunt_elixir, ProducthuntElixirWeb.Endpoint,
  http: [port: 4001],
  server: true

config :producthunt_elixir, :sql_sandbox, true

config :wallaby, screenshot_on_failure: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :producthunt_elixir, ProducthuntElixir.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DATABASE_USERNAME"),
  password: System.get_env("DATABASE_PASSWORD"),
  database: "producthunt_elixir_test",
  hostname: System.get_env("DATABASE_HOST"),
  pool: Ecto.Adapters.SQL.Sandbox
